module.exports.csrf = function csrf (req,res,next) {
	res.locals.token  = req.csrfToken;
	console.log("csrf set");
	next();
}

module.exports.authenticated = function authenticated (req,res,next) {
	res.locals.isAuthenticated = req.session.isAuthenticated;
	if(req.session.isAuthenticated){
		res.locals.user = req.session.user;
	}
	next();
};

module.exports.requireAuthentication = function requireAuthentication (req,res,next) {
	if(req.session.isAuthenticated){
		next();
	}
	else{
		res.redirect('/login');
	}
};

module.exports.auth = function auth (username,password,session) {
	var Auth = username==='Ashwin'||username==='Udayakumar';
	if(Auth){
		session.isAuthenticated = Auth;
		session.user = {username:username};
	}
	console.log("returning auth val:"+Auth);
	return Auth;
};

module.exports.logout = function logout (session) {
	session.isAuthenticated = false;
	delete session.user;
};