var express = require('express')
var app  = express()
var routes = require('./routes')
var errorhandle = require('./middleware/errorhandle');
var log = require('./middleware/log');
var partials = require('express-partials');
var cookie = require('cookie-parser');
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var bodyparser = require('body-parser');
var csrf = require('csurf');
var util = require('./middleware/utilities');
app.set('view options',{defaultLayout:'layout'});
app.set('view engine','ejs');

app.use(partials());
app.use(log.logger);
app.use(express.static(__dirname+'/static'));
app.use(cookie('secret'));
app.use(session({secret:'secret',
	saveUninitialized: true,
	resave: true,
	store:new RedisStore(
		{url:'redis://localhost'})
	}));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
//var csrfProtection = csrf({ cookie: true });
//app.use(csrf({cookie:true}));
//app.use(util.csrf);
app.use(util.authenticated);
app.get('/',routes.index);
app.get('/login',routes.login)
app.post('/login',routes.loginProcess)
app.get('/chat',[util.requireAuthentication],routes.chat);
app.get('/logout',function  (req,res) {
	console.log("to be logged out");
	routes.logout;
});
app.get('/error',function(req,res,next){
	next(new Error("Some Error"));
});
app.use(errorhandle.notFound);
app.use(errorhandle.error);
app.listen(4000);
console.log("Server is running on port 4000");