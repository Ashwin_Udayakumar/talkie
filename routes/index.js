var util = require('../middleware/utilities');
module.exports.index = index;
module.exports.login = login;
module.exports.loginProcess = loginProcess;
module.exports.chat = chat;
function index (req,res) {
	res.cookie("Ind-Cookie","This is from Index");
	//res.clearCookie('Ind-Cookie');
	res.render('index',{title:'Index'});
};
function login (req,res) {
	console.log("in the login");
	res.render("login",{layout:'layout',title:'LoginPage'});
};
function loginProcess (req,res) {
	console.log("in loginProcess");
	var isauth = util.auth(req.body.username,req.body.password,req.session);
	console.log(req.body.username);
	if(isauth)
		res.redirect('/chat');
	else
		res.redirect('/login');
};
function logout (req,res) {
	console.log("logging out");
	util.logout(req.session);
	res.redirect('/');
};
function chat(req,res) {
	res.render("chat",{layout:'layout',title:'ChatRoom'});
};
